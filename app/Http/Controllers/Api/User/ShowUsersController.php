<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Mock\Data;

class ShowUsersController extends Controller
{
    public function __invoke(Request $request)
    {
        return response()->json(Data::getUsers());
    }
}
