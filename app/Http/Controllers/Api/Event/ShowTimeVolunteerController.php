<?php

namespace App\Http\Controllers\Api\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Mock\Data;

class ShowTimeVolunteerController extends Controller
{
    public function __invoke(Request $request)
    {
        return response()->json(Data::getTimeVolunteers());
    }
}
