<?php

namespace App\Http\Controllers\Api\Participant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Mock\Data;

class ShowParticipantsController extends Controller
{
    public function __invoke(Request $request)
    {
        return response()->json(Data::getParticipants());
    }
}
