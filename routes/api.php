<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace' => 'Api\User'], function(){
    Route::get('users', 'ShowUsersController')->name('api/users');
});

Route::group(['namespace' => 'Api\Event'], function(){
    Route::get('treasure-volunteers', 'ShowTreasureVolunteerController')->name('api/treasure-volunteers');
    Route::get('time-volunteers', 'ShowTimeVolunteerController')->name('api/time-volunteers');
});

Route::group(['namespace' => 'Api\Participant'], function(){
    Route::get('participants', 'ShowParticipantsController')->name('api/participants');
});
